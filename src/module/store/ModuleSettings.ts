export const SAVE_SETTINGS_REVISION = 1;

export enum ModuleSetting {
  // QUICKOPEN = "quickOpen", // dead setting
  ENABLE_GLOBAL_CONTEXT = "enableGlobalContext",
  INDEXING_DISABLED = "indexingDisabled",
  FILTERS_CLIENT = "filtersClient",
  FILTERS_WORLD = "filtersWorld",
  FILTERS_SHEETS = "filtersSheets",
  FILTERS_SHEETS_ENABLED = "filtersSheetsEnabled",
  GM_ONLY = "gmOnly",
  AUTOMATIC_INDEXING = "automaticIndexing",
  INDEX_TIMEOUT = "indexTimeout",
  SEARCH_BUTTON = "searchButton",
  KEY_BIND = "keyBind",
  DEFAULT_ACTION_SCENE = "defaultSceneAction",
  DEFAULT_ACTION_ROLL_TABLE = "defaultActionRollTable",
  DEFAULT_ACTION_MACRO = "defaultActionMacro",
  SEARCH_TOOLTIPS = "searchTooltips",
  EMBEDDED_INDEXING = "embeddedIndexing",
}
